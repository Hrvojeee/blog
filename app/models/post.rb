class Post < ActiveRecord::Base
  attr_accessor :body, :title
  has_many :comments, dependent: :destroy
  validates_presence_of :title
  validates_presence_of :body
end
