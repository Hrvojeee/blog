class Comment < ActiveRecord::Base
  attr_accessor :body, :post_id
  belongs_to :post
  validates_presence_of :post_id
  validates_presence_of :body

end
